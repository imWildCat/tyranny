class MoneyRecord < ApplicationRecord

    before_create :set_balance

    validates :category, presence: true
    validates :amount, presence: true

    def set_balance
        # puts "set_balance"
        last_record = MoneyRecord.order(id: :desc).first
        # puts "last balance: #{last_record&.balance} + #{self.amount}"
        # new_balance = (last_record&.balance || 0) + self.amount 
        # puts "new balance #{new_balance}"
        self.balance = (last_record&.balance || 0) + self.amount 
        
    end

end
