class Api::V1::MoneyRecordsController < ApplicationController
  before_action :set_money_record, only: [:show, :update, :destroy]

  # GET /money_records
  def index
    @money_records = MoneyRecord.order(id: :desc)

    render json: @money_records
  end

  # For async
  def fetch
    time = Time.parse(params[:updated_at]) + 1.second
    @money_records = MoneyRecord.order(updated_at: :asc).where('updated_at >= ?', time)

    # p 'Count:'
    # p @money_records.count

    render json: @money_records
  end
  

  # GET /money_records/1
  def show
    render json: @money_record
  end

  # POST /money_records
  def create
    @money_record = MoneyRecord.new(money_record_params)

    p = money_record_params

    if @money_record.save
      render json: @money_record, status: :created, location: [:api, :v1, @money_record]
    else
      render json: @money_record.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /money_records/1
  def update
    if @money_record.update(money_record_params)
      render json: @money_record
    else
      render json: @money_record.errors, status: :unprocessable_entity
    end
  end

  # DELETE /money_records/1
  def destroy
    @money_record.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_money_record
      @money_record = MoneyRecord.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def money_record_params
      params.require(:money_record).permit(:category, :amount, :note, :is_expense, :created_at)
    end
end
