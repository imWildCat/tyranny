class CreateMoneyRecords < ActiveRecord::Migration[5.2]
  def change
    create_table :money_records do |t|
      t.string :category, null: false
      t.boolean :is_expense, null: false, default: true
      t.integer :amount, null: false
      t.integer :balance, null: false
      t.string :note

      t.timestamps
    end
  end
end
