require 'test_helper'

class Api::V1::MoneyRecordsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @money_record = money_records(:one)
  end

  test "should get index" do
    get api_v1_money_records_url, as: :json
    assert_response :success
  end

  test "should create money_record" do
    assert_difference('MoneyRecord.count') do
      post api_v1_money_records_url, params: { money_record: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show money_record" do
    get api_v1_money_record_url(@money_record), as: :json
    assert_response :success
  end

  test "should update money_record" do
    patch api_v1_money_record_url(@money_record), params: { money_record: {  } }, as: :json
    assert_response 200
  end

  test "should destroy money_record" do
    assert_difference('MoneyRecord.count', -1) do
      delete api_v1_money_record_url(@money_record), as: :json
    end

    assert_response 204
  end
end
