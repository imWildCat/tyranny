Rails.application.routes.draw do
  root to: 'home#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  namespace :api do
    namespace :v1 do
      resources :money_records do
        collection do
          get '/fetch', to: 'money_records#fetch'
        end
      end
    end
  end

end
